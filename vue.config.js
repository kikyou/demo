const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: config => {
    config.module
        .rule('css')
        .oneOf('vue')
        .use('postcss-loader')
        .loader('postcss-loader')
        .end()
  }
})
